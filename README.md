# Python Week Of Code, Namibia 2019

To launch the Django web server,
run

```
python manage.py runserver
```

We will ignore

```
You have 14 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
```

for now.

Open [http://localhost:8000/](http://localhost:8000/) in your web browser.

[blog/templates/blog/index.html](blog/templates/blog/index.html) has the source code of the page that you see in your web browser.

## Task for Instructor

1. Add the attribute `style="background: red; color: white;"` to the `<h1>` element.
2. Add the attribute `style="font-style: italic;"` to the `<p>` element.
3. Remove the attributes introduced before and add

   ```
   <style>
     h1 {
         background: red;
         color: white;
     }

     p {
         font-style: italic;
     }
   </style>
   ```

   to the `<head>` element.

## Tasks for Learners

1. Add a new element `<p>`.
2. Add a new element `<h2>`.
3. Customise the style of the element `<h2>`.